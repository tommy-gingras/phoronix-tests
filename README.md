# phoronix-tests

Custom Test Suite for Phoronix

Manual : https://www.phoronix-test-suite.com/documentation/phoronix-test-suite.pdf

HOW-TO : https://kontron.atlassian.net/wiki/spaces/IHS/pages/164462617/Benchmark

Debian/ contain installation script for phoronix

If you want to add a new test suite to the add-test-suite.sh script:
FILES={"kontron-cpu" "my-new-test-suite"}

If you want to update the version of the installed phoronix, in each OS directory:
change each lines that mention phoronix-X-X-X.deb