#!/bin/bash

## Author : Tommy Gingras
## Date : 2017-12-22
## Goal : Install phoronix and its dependencies on Centos based systems
## Version : 1.0.0

## Check if it run as ROOT
if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
fi

# Variable definition
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Function definition
quit() {
        exit
}

check_connectivity(){
        echo -e "Checking network connectivity..."
        ping google.com -c 2 &> /dev/null
        check_error $? "Internet connection is required."
}

check_error() {
        if [[ $1 -ne 0 ]]
        then
                echo -e "${RED}An error occur during the installation process.${NC}"
                echo $2
                quit
        fi
}

## PROCEDURE ##

# Check if ethernet is available
check_connectivity

# Install dependencies
yum install -y php-cli php7.0-cli php-xml unzip
check_error $? "Failed to install the dependencies."

# Download and install Phoronix
if [ ! -f ./phoronix-test-suite-7.6.0.tar.gz ]; then
        wget http://phoronix-test-suite.com/releases/phoronix-test-suite-7.6.0.tar.gz
        check_error $?
fi

if [ ! -d ./phoronix-test-suite-7.6.0 ]; then
        tar -xvzf phoronix-test-suite-7.6.0.tar.gz
        check_error $?
else
        echo "Already exist"
fi

# Install Phoronix
cd phoronix-test-suite
./install-sh
check_error $? "Failed to install phoronix."

cd ..
rm -R phoronix-test-suite/

echo "------------------------"
echo -e "Installation Done."