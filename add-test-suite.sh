#!/bin/bash

## Author : Tommy Gingras
## Date : 2017-12-19
## Goal : Move the custom test suite files
## Version : 1.0.0

# Variable definition
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Function definition
quit() {
	exit
}

check_error() {
	if [[ $1 -ne 0 ]]
	then
		echo -e "${RED}An error occur during the process.${NC}"
		echo $2
		quit
	fi
}

check_install(){
	if [ ! -d /usr/share/phoronix-test-suite/ ]; then
		echo -e "${RED} You have to install phoronix before copying the test suite.${NC}"
		quit
	fi
}

# Before go further, validate the installation
check_install

FILES=("kontron-cpu" "kontron-hard-drive" "kontron-io" "kontron-memory")

# determine if the installation if in the home or in the /var/lib...

# Copy all the files
if [ -d $HOME/.phoronix-test-suite/test-suites/local ]; then
	echo "Copying all test suites in your home directory"
    for file in "${FILES[@]}"; do
		cp -R $file $HOME/.phoronix-test-suite/test-suites/local/
		check_error $? "File $file copied [${RED}FAIL${NC}]"
		echo -e "File $file copied [${GREEN}OK${NC}]"
	done
elif [ -d /var/lib/phoronix-test-suite/test-suites/local/ ]; then
	echo "Copying all test suites in the installation directory"
	for file in "${FILES[@]}"; do
		cp -R $file /var/lib/phoronix-test-suite/test-suites/local/
		check_error $? "File $file copied [${RED}FAIL${NC}]"
		echo -e "File $file copied [${GREEN}OK${NC}]"
	done
else
	echo -e "${RED} You have to execute phoronix at least one time before copying the files to accept the user agreement.${NC}"
	echo -e "command: phoronix-test-suite and answer the questions, after re-run this script."
	quit
fi

