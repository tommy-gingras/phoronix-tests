#!/bin/bash

## Author : Tommy Gingras
## Date : 2017-12-19
## Goal : Install phoronix and its dependencies on Debian based systems
## Version : 1.0.0

## Check if it run as ROOT
if [[ $EUID -ne 0 ]]; then
  echo "You must be a root user" 2>&1
  exit 1
fi

# Variable definition
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m' # No Color

# Function definition
quit() {
	exit
}

check_connectivity(){
	echo -e "Checking network connectivity..."
	ping google.com -c 2 &> /dev/null
	check_error $? "Internet connection is required."
}

check_error() {
	if [[ $1 -ne 0 ]]
	then
		echo -e "${RED}An error occur during the installation process.${NC}"
		echo $2
		quit
	fi
}

## PROCEDURE ##

# Check if ethernet is available
check_connectivity

# Update 
apt-get update
check_error $? "Failed to update the apt-get."

# Install dependencies
apt-get install -f -y php-cli php7.0-cli php-xml unzip
check_error $? "Failed to install the dependencies."

# Download and install Phoronix
if [ ! -f ./phoronix-test-suite_7.6.0_all.deb ]; then
    wget http://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_7.6.0_all.deb
	check_error $?
fi

# Install Phoronix
dpkg -i phoronix-test-suite_7.6.0_all.deb
check_error $? "Failed to install phoronix."

echo "------------------------"
echo -e "Installation Done."



